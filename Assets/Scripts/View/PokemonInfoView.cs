﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Virti.Interface;
using Virti.Model;
#pragma warning disable 649
namespace Virti.View
{
    public class PokemonInfoView : MonoBehaviour, ITextureLoadListener 
    {
        public string FileName => pokemonDataModel.name;
        public string Url => pokemonDataModel.image; 
        public bool IsActive => isActive;

        [SerializeField] GameObject root;
        [SerializeField] Image image; 
        [SerializeField] TMP_Text[] txtAttaksFast;
        [SerializeField] TMP_Text[] txtAttaksSpecial;

        PokemonDataModel pokemonDataModel;
        bool isActive;
        public void SetAcive(bool value)
        {
            isActive = value;
            root.SetActive(value);
        }
        public void Load(PokemonDataModel pokemonDataModel)
        {
            this.pokemonDataModel = pokemonDataModel;
            image.color = Color.white;

            if (pokemonDataModel.attacks != null)
            {
                if (pokemonDataModel.attacks.fast != null)
                    for (int i = 0; i < txtAttaksFast.Length; i++)
                    {
                        txtAttaksFast[i].text = (pokemonDataModel.attacks.fast.Length > i) ? pokemonDataModel.attacks.fast[i].name : "";
                    }

                if (pokemonDataModel.attacks.special != null)
                    for (int i = 0; i < txtAttaksSpecial.Length; i++)
                    {
                        txtAttaksSpecial[i].text = (pokemonDataModel.attacks.special.Length > i) ? pokemonDataModel.attacks.special[i].name : "";
                    }
            }
            SetAcive(true);
        }

        public void OnLoadComplete(ThumbnailTexture texture)
        {
            if (texture == null)
            {
                image.sprite = null;
                image.transform.localScale = Vector3.one;
            }
            else
            {
                image.sprite = texture.SpriteData; 
                if (texture.AspectRatio > 1)
                    image.transform.localScale = new Vector3(1, 1 / texture.AspectRatio, 1);
                else
                    image.transform.localScale = new Vector3(texture.AspectRatio, 1, 1);
            }
        }
    }
}

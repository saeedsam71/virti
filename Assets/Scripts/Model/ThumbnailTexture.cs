﻿ 
using UnityEngine;
namespace Virti.Model
{
    public class ThumbnailTexture
    {
        public Sprite SpriteData { get; private set; }
        public float AspectRatio { get; private set; }

        public ThumbnailTexture(Texture2D texture, float aspectRatio)
        {
            this.SpriteData = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            this.AspectRatio = aspectRatio;
        }
    }
}
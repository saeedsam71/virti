﻿ 
namespace Virti.Model
{
    public class RequestResponseModel
    {
        public PokemonResponseModel data;
    }
     
    public class PokemonResponseModel
    {
        public PokemonDataModel pokemon;
    }

    public class PokemonDataModel
    {
        public string name;
        public string image;
        public PokemonAttackModel attacks;
    }

    public class PokemonAttackModel
    {
        public PokemonAttackInfoModel[] fast;
        public PokemonAttackInfoModel[] special;
    }

    public class PokemonAttackInfoModel
    {
        public string name;
    }
}
﻿
using System.Collections.Generic;
using UnityEngine;
using Virti.Interface;
using Virti.View;
using Zenject;
#pragma warning disable 649
namespace Virti.Controller
{
    public class PoolController : MonoBehaviour, IPoolController
    {
        [SerializeField] PokemonInfoView pokemonViewPrefab; 
        [SerializeField] Transform pokemonItemParent;
        [SerializeField] int poolSize = 50;
        List<PokemonInfoView> allPokemonInfoView;
        [Inject]
        public void Init()
        {
            allPokemonInfoView = new List<PokemonInfoView>();
            for (int i = 0; i < poolSize; i++)
            {
                MakeNewPokemonView();
            }
        }

        PokemonInfoView MakeNewPokemonView()
        {
            var pokemonView = Instantiate(pokemonViewPrefab, pokemonItemParent);
            pokemonView.SetAcive(false);
            allPokemonInfoView.Add(pokemonView);
            return pokemonView;
        }

        public PokemonInfoView GetPokemonInfoView()
        {
            var free = allPokemonInfoView.Find(x => !x.IsActive);
            if (free == null)
            {
                free = MakeNewPokemonView();
            }
            return free;
        }
    }
}

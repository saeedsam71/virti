﻿using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using Virti.Interface;
using Virti.Model;
using Zenject;
#pragma warning disable 649
namespace Virti.Controller
{
    public class IOManager : MonoBehaviour,  IIOManager
    {
        string root;
        readonly string ThumbnailsDirectory = "Thumbnail";
        readonly string Extention = ".png";
        readonly string AspectRatio = "AspectRatio";

        [Inject]
        public void Init()
        {
            root = Path.Combine(Application.persistentDataPath, ThumbnailsDirectory);
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
        }
        public void SaveThumbnail(string fileName, float aspectRatio, byte[] bytes)
        {
            FileStream file = File.Open(Path.Combine(root, fileName + Extention), FileMode.Create);
            BinaryWriter binary = new BinaryWriter(file);
            binary.Write(bytes);
            binary.Close();
            binary.Dispose();
            file.Close();
            file.Dispose();
            PlayerPrefs.SetFloat(fileName + AspectRatio, aspectRatio);
        }
          
        public ThumbnailTexture LoadImage(string fileName)
        {
            string fullPath = Path.Combine(root, fileName + Extention);
            if (File.Exists(fullPath))
            {
                byte[] bytes = File.ReadAllBytes(fullPath);
                Texture2D texture = new Texture2D(1, 1, TextureFormat.DXT5, false);
                texture.LoadImage(bytes); 
                return new ThumbnailTexture(texture, PlayerPrefs.GetFloat(fileName + AspectRatio, 1));
            }
            return null;
        }

        public T LoadData<T>(string name)
        {
            if (PlayerPrefs.HasKey(name))
            {
                string data = PlayerPrefs.GetString(name, "");
                if (!string.IsNullOrEmpty(data))
                    return JsonConvert.DeserializeObject<T>(data);
            }
            return default(T);
        }

        public void SaveData(string name, object data)
        {
            PlayerPrefs.SetString(name, JsonConvert.SerializeObject(data));
        }
    }

}
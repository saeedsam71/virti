﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using Virti.Controller;
using Virti.Interface;
using Virti.Model;
using Virti.View;
using Zenject;
#pragma warning disable 649
namespace Virti
{
    public class PokemonSceneController : MonoBehaviour
    { 
        [SerializeField] string[] pokemons;
        [Inject]
        IFileDownloadManager fileDownloadManager;
        [Inject]
        IPokemonServerBridge pokemonServerBridge;
        [Inject]
        IPoolController poolController;
        
         
        public void LoadNext()
        {
            for (int i = 0; i < pokemons.Length; i++)
            {
                pokemonServerBridge.LoadPokemon(pokemons[i], OnDataLoadComplete);
            } 
        }

        private void OnDataLoadComplete(string pokemonName, PokemonDataModel pokemonDataModel)
        {
            if (pokemonDataModel != null)
            {
                var pokemonView = poolController.GetPokemonInfoView();
                pokemonView.Load(pokemonDataModel);
                fileDownloadManager.DownLoadThumbnail(pokemonView);
            } 
        }
    }
}

﻿using Newtonsoft.Json; 
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using Virti.Interface;
 
namespace Virti.Controller
{
    public class WebRequestController : MonoBehaviour,  IWebRequestController
    {
        public async Task<T> CallWebRequestAsync<T>(string requestUriString)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                return default(T);
            } 
            try
            { 
                var getRequest = UnityWebRequest.Get(requestUriString);
                await getRequest.SendWebRequest(); 
                var jsonResponse = getRequest.downloadHandler.text; 
                getRequest.Dispose(); 
                return JsonConvert.DeserializeObject<T>(jsonResponse);
            }
            catch
            {
                return default(T);
            }
        }
    }

    public static class ExtensionMethods
    {
        public static TaskAwaiter GetAwaiter(this AsyncOperation asyncOp)
        {
            var tcs = new TaskCompletionSource<object>();
            asyncOp.completed += obj => { tcs.SetResult(null); };
            return ((Task)tcs.Task).GetAwaiter();
        }
    }


}

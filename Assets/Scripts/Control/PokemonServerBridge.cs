﻿
using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using Virti.Interface;
using Virti.Model;
using Zenject;

#pragma warning disable 649
namespace Virti.Controller
{
    public class PokemonServerBridge : MonoBehaviour, IPokemonServerBridge
    {
        readonly string BaseUrl = "https://graphql-pokemon2.vercel.app";
        [Inject]
        IWebRequestController webRequestController;
        [Inject]
        IIOManager ioManager;
        [Inject]
        IPokemonServerBridge pokemonServerBridge;

        string MakeQueryString(string name)
        {
            return "/?query={ pokemon(name: \"" + name + "\") {name, image, attacks { fast { name }, special { name } } }}";
        }
         
        private IEnumerator WaitFor(string pokemon, Task<RequestResponseModel> task, Action<string, PokemonDataModel> onComplete)
        {
            while (!task.IsCompleted)
                yield return new WaitForEndOfFrame();

            
            var responseModel = task.Result;
            if (responseModel == null)
            {
                responseModel = ioManager.LoadData<RequestResponseModel>(pokemon);
            }
            else
            {
                if (responseModel.data != null)
                    ioManager.SaveData(pokemon, responseModel);
            }
            if (onComplete != null)
            {
                if (responseModel != null && responseModel.data != null)
                    onComplete(pokemon, responseModel.data.pokemon);
                else
                    onComplete(pokemon, null);
            } 
        }
        public void LoadPokemon(string pokemon, Action<string, PokemonDataModel> onComplete)
        {
            var task = webRequestController.CallWebRequestAsync<RequestResponseModel>(BaseUrl + MakeQueryString(pokemon));
            task.ConfigureAwait(false);
            StartCoroutine(WaitFor(pokemon, task, onComplete)); 
        } 
    } 
}
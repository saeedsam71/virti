﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using Virti.Interface;
using Virti.Model;
using Zenject;
#pragma warning disable 649
namespace Virti.Controller
{
    public class FileDownloadManager : MonoBehaviour, IFileDownloadManager
    {
        [Inject]
        IIOManager fileManager;
        static readonly int ThumbnailWidth = 256;
        static readonly int ThumbnailHeight = 256;

        public void DownLoadThumbnail(ITextureLoadListener listener, int width, int height)
        {
            StartCoroutine(CoroutinDownLoadThumbnail(listener, width, height));
        }
        public void DownLoadThumbnail(ITextureLoadListener listener)
        {
            StartCoroutine(CoroutinDownLoadThumbnail(listener, ThumbnailWidth, ThumbnailHeight));
        }
        IEnumerator CoroutinDownLoadThumbnail(ITextureLoadListener listener, int width, int height)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                ThumbnailTexture texture = fileManager.LoadImage(listener.FileName);
                OnLoadComplete(texture, listener);
            }
            else
            {
                UnityWebRequest www = UnityWebRequestTexture.GetTexture(listener.Url);
                yield return www.SendWebRequest();
                while (!www.isDone)
                {
                    yield return 0;
                }
                if (www.error == null)
                {
                    Texture2D texture = DownloadHandlerTexture.GetContent(www);
                    float aspectRatio = (float)texture.width / (float)texture.height;
                    if (texture != null && (texture.width != width || texture.height != height))
                    {
                        texture = Helper.ScaleTexture(texture, width, height);
                    }
                    OnLoadComplete(new ThumbnailTexture(texture, aspectRatio), listener);
                    if (texture != null)
                    {
                        fileManager.SaveThumbnail(listener.FileName, aspectRatio, texture.EncodeToPNG()); ;
                    }
                }
                www.downloadHandler.Dispose();
                www.Dispose();
            }
        }

        void OnLoadComplete(ThumbnailTexture texture, ITextureLoadListener listener)
        {
            if (listener != null)
            {
                listener.OnLoadComplete(texture);
            }
        }

    }
}

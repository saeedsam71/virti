﻿using UnityEngine;
using Virti.Controller;
using Virti.Interface;
using Zenject;

namespace Virti
{
    [RequireComponent(typeof(IOManager), typeof(PokemonServerBridge), typeof(WebRequestController))]
    [RequireComponent(typeof(FileDownloadManager), typeof(PoolController))]
    public class DependensyInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IIOManager>().FromInstance(GetComponent<IOManager>());
            Container.Bind<IPokemonServerBridge>().FromInstance(GetComponent<PokemonServerBridge>());
            Container.Bind<IWebRequestController>().FromInstance(GetComponent<WebRequestController>());
            Container.Bind<IFileDownloadManager>().FromInstance(GetComponent<FileDownloadManager>());
            Container.Bind<IPoolController>().FromInstance(GetComponent<PoolController>());
        }

    }

}

﻿
namespace Virti.Interface
{
    public interface IFileDownloadManager
    { 
        void DownLoadThumbnail(ITextureLoadListener listener);
    }
}


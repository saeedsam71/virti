﻿using System.Threading.Tasks;

namespace Virti.Interface
{
    public interface IWebRequestController
    {
         Task<T> CallWebRequestAsync<T>(string requestUriString);
    }
}

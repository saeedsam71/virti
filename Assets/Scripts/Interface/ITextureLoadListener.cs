﻿
using Virti.Model;

namespace Virti.Interface
{
    public interface ITextureLoadListener
    {
        string FileName { get; }
        string Url { get; }
        void OnLoadComplete(ThumbnailTexture texture);
    }
}
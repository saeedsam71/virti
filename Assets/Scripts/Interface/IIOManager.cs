﻿
using Virti.Model;

namespace Virti.Interface
{
    public interface IIOManager
    {
        void Init();
        void SaveThumbnail(string fileName, float aspectRatio, byte[] bytes);
        ThumbnailTexture LoadImage(string fileName);
        T LoadData<T>(string name);
        void SaveData(string name, object data);
    }
}

﻿
using Virti.View;

namespace Virti.Interface
{
    public interface IPoolController
    {
        void Init();
        PokemonInfoView GetPokemonInfoView();
    }
}

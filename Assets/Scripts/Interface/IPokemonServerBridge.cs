﻿using System;
using Virti.Model;

namespace Virti.Interface
{
    public interface IPokemonServerBridge
    {
        void LoadPokemon(string pokemon, Action<string, PokemonDataModel> onComplete);
    }
}
